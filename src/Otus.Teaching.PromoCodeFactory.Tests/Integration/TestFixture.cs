﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.Tests.Integration
{
    public static class TestFixture
    {
        public static readonly IHost Builder = CreateBuilder();

        private static IHost CreateBuilder()
        {
            var builder = CreateHostBuilder(null).Build();

            using var serviceScope = builder.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<DataContext>();

            context?.Database.Migrate();

            var dbInitializer = serviceScope.ServiceProvider.GetService<IDbInitializer>();
            dbInitializer?.InitializeDb();

            return builder;
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}
