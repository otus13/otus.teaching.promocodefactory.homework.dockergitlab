using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.Tests.Integration
{
    public class EmployeesControllerTests
    {
        [Fact]
        public async Task GetEmployees_returns_correct_number_of_records_from_FakeDataFactory()
        {
            var builder = TestFixture.Builder;

            var employeesController = ActivatorUtilities.CreateInstance<EmployeesController>(builder.Services);

            var result = await employeesController.GetEmployeesAsync();

            Assert.Equal(2, ((IList)result).Count);
        }
    }
}
